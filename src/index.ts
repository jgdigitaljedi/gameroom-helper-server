import Koa, { Context } from 'koa';
import Router from 'koa-router';
import json from 'koa-json';
import logger from 'koa-logger';
import cors from '@koa/cors';

const app = new Koa();
const router = new Router();

router.get('/', async (ctx: Context) => console.log('server works', ctx));

app.use(cors());
app.use(json());
app.use(logger());
app.use(router.routes()).use(router.allowedMethods());

app.listen(3000, () => console.log('Koa started'));
